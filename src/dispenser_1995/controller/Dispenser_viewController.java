/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dispenser_1995.controller;

import dispenser_1995.model.Message;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import kea.zmirc.systemintegration.p1.shared.DatagramReader;
import kea.zmirc.systemintegration.p1.shared.UDPSocket;
import kea.zmirc.systemintegration.p1.shared.UDPSocketReceiveHandler;
import kea.zmirc.systemintegration.p1.shared.util.User;

/**
 * FXML Controller class
 *
 * @author FILIP
 */
public class Dispenser_viewController implements Initializable {
    @FXML
    private Pane main_pane;
    @FXML
    private Label status_label;
    @FXML
    private TextField price;
    @FXML
    private TextField volume;
    @FXML
    private TextField price_per_liter;
    @FXML
    private RadioButton nozzle_hang;
    @FXML
    private RadioButton nozzle_grab;
    @FXML
    private ToggleButton night_mode_onoff;
    @FXML
    private ToggleButton start_stop_fuel;
    
    private ToggleGroup toggle_group;
    
    private Double current_volume = 0.00;
    private Double price_liter = 11.75;
    private Double current_price = 0.00;
    
    private double max_price = 0.0;
    private double fuel_flow_tick;
    
    private final String FUELING = "FUEL";
    private final String IDLE = "IDLE";
    private final String CALLING = "CALL";
    private final String AWAITS_TOTAL_READY = "ATUP";
    
    private String state;
    
    private boolean is_night_mode = false;
    private boolean is_locked;
    private boolean is_fueling = false;
    private boolean done_fueling= false;
    
    private UDPSocket s;
    
    //Timeline for fueling
    private Timeline fueling_timeline;
    private KeyFrame fueling_keyframe;
    
    //Message variables
    private String message;

    //Command bytes
    private static final byte change_mode = (byte) 0x41;
    private static final byte change_lock = (byte) 0x42;
    private static final byte set_price_per_liter = (byte) 0x46;
    private static final byte get_status = (byte) 0x47;
    private static final byte current_volume_byte = (byte) 0x48;
    private static final byte current_state = (byte) 0x49;
    private static final byte start_fueling = (byte) 0x53;
    
    private Message decoded_message;
    private MessageHandler mh;
    
    private Message current_volume_message;
    private Message current_state_message;
    
    private final DecimalFormat price_df = new DecimalFormat("######.##");
    private final DecimalFormat volume_df = new DecimalFormat("########.##");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        state = IDLE;
        
        //Class to handle messages send between POS and Pump
        mh = new MessageHandler();     
        
        //Adding radio buttons to toggle group
        toggle_group = new ToggleGroup();
        toggle_group.getToggles().add(nozzle_hang);
        toggle_group.getToggles().add(nozzle_grab);
        
        //Initializing messages to save memory on creating new instances while sending info to POS.
        current_state_message = new Message();
        current_volume_message = new Message();
        
        //Starting texts in text fields.
        volume.setText(volume_df.format(current_volume));
        price.setText(price_df.format(current_price));
        price_per_liter.setText(price_df.format(price_liter));
        
        //Getting focus fueling button
        start_stop_fuel.requestFocus();
        
        //Primarly nozzle is hanged
        nozzle_hang.setSelected(true);

     @SuppressWarnings("unchecked")

      UDPSocket s = new UDPSocket(User.Conrad_Lasse, new UDPSocketReceiveHandler() {
           
        public void onReceive(DatagramReader dr, UDPSocket udps) {
            System.out.println("DISPENSER");
            System.out.println(dr.getPv());
//            System.out.println(dr.getSource());
//            System.out.println(dr.getSourceAddress().toString());
//            System.out.println(dr.getSourcePort());
//            System.out.println(dr.getDestination());
            System.out.println(Arrays.toString(dr.getData()));
            System.out.println("DISPENSER END");
            
            byte[] data = dr.getData();
            
            try {
                
                decoded_message = null;                
                decoded_message = mh.DecodeMessage(data);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(Dispenser_viewController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            try {
                ReactToMessage(decoded_message.getCommand(), decoded_message.getData());
            } catch (UnknownHostException ex) {
                Logger.getLogger(Dispenser_viewController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }, 10_002, UDPSocket.DEFAULT_BUFFER_SIZE, true);    
     
     this.s = s;
    }    
  
    public void ReactToMessage(byte command, String data) throws UnknownHostException {
        
        switch (command) {
                    
                    case change_lock :  //LOCK UNLOCK
                        SetLock(data);
                        
                    break;
                        
                    case get_status :  //LOCK UNLOCK
                        
                        SendStatus();
                        
                     break;
                        
                    case change_mode :  // DAY NIGHT
                        
                        SetMode(data);
                     break;
                        
                    case set_price_per_liter :  //WHAT IS SAYS
                        
                        SetPricePerLiter(data);
                     break;
                     
                    case start_fueling :  // WHAT IS SAYS
                        Platform.runLater(new Runnable() {
                            public void run() {
                                state = IDLE;
                                start_stop_fuel.setDisable(false);
                                max_price = Double.parseDouble(data);
                            }
                        });              
                     break;
                        
                    default : 
                        System.out.println("Default shit");
                     break;
                }
            }

    
    @FXML
    private void SwitchNightMode(ActionEvent event) {
        //Nothing to do.

    }
    
    @FXML
    private void NozzleGrabbed(ActionEvent event) {

        price.setText("00.00");
        volume.setText("00.00");
            
        if(is_night_mode) {
            state = CALLING;
        } else {
        
           start_stop_fuel.setDisable(false);
           status_label.setText("WAITING");
           
        }
    }
    
    @FXML
    private void NozzleHang(ActionEvent event) {
            
        if(current_volume != 0.0) {
            state = AWAITS_TOTAL_READY;
            SetLock("LOC");
        }
        
        if(is_night_mode) {
            start_stop_fuel.setDisable(true);
        }

    }

    @FXML
    private void StartStopFuel(ActionEvent event) throws UnknownHostException {
        

        if(fueling_timeline != null ) {
            
            fueling_timeline.stop();
            fueling_timeline = null;
            
            is_fueling = false;
            SendCurrentVolume();
                        
            nozzle_hang.setDisable(false);
            nozzle_grab.setDisable(false);
            
            if(done_fueling) {
                start_stop_fuel.setDisable(true);
                done_fueling = false;
            }
            
        } else {
            
            state = FUELING;
            is_fueling = true;
            
            nozzle_hang.setDisable(true);
            nozzle_grab.setDisable(true);
            
            Random r = new Random();
            
            fueling_timeline = new Timeline();
            fueling_timeline.cycleCountProperty().set(Animation.INDEFINITE);

            fueling_keyframe = new KeyFrame (Duration.millis(200), new EventHandler<ActionEvent> () {

                @Override
                public void handle(ActionEvent event) {
                    
                    fuel_flow_tick = r.nextDouble() / 2;
                    
                    current_volume += fuel_flow_tick;
                    current_price = Double.parseDouble(volume_df.format(current_volume).replace(",", ".")) * price_liter;
                    
                    if(max_price != 0.0 && current_price + price_liter >= max_price) {
                        
                        double left = max_price - current_price;
                        
                        current_volume += left/ price_liter;
                        current_price += left;
                        
                        done_fueling = true;
                        
                        try {
                            StartStopFuel(event);
                        } catch (UnknownHostException ex) {
                            Logger.getLogger(Dispenser_viewController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    
                    volume.setText(volume_df.format(current_volume));
                    price.setText(volume_df.format(current_price));
                    
                }
            });   

            fueling_timeline.getKeyFrames().add(fueling_keyframe);
            fueling_timeline.play();
        }
    }
    
    public void SetLock(String data) {
        //POS->DIS 
        switch (data) {
            case "UNL" : 
                Platform.runLater(new Runnable() {
                    public void run() {
                        status_label.setText("Unlocked");
                        status_label.setTextFill(Color.GREEN);
                        price.setText("00.00");
                        volume.setText("00.00");
                        current_price = 0.0;
                        current_volume = 0.0;
                        state = IDLE;       
                        nozzle_grab.setDisable(false);
                        start_stop_fuel.setDisable(true); 
                        
                    }
                });
                break;
            
            case "LOC" : 
                Platform.runLater(new Runnable() {
                    public void run() {
                        status_label.setText("Locked");
                        status_label.setTextFill(Color.RED);
                        nozzle_grab.setDisable(true);
                        start_stop_fuel.setDisable(true);
                       
                    }
                });
               break;
                        
            default :

                break;
        }
    }

    
    public void SetPricePerLiter (String data) {
        //POS->DIS
        
        Double new_price = Double.parseDouble(data);
        
            Platform.runLater(new Runnable() {
                public void run() {        
                    price_liter = new_price;
                    price_per_liter.setText(price_liter+"");
                }
            });
    }
    
    public void SetMode(String data) {
        
        switch (data) {
            case "DAY" :
                    SetModeGUI(false);
                    
                    break;
            case "NIG" :
                    SetModeGUI(true);
                    break;
            default :
                
                break;
    }
        
        
    }
    
    private void SetModeGUI(boolean value) {
        Platform.runLater(new Runnable() {
            public void run() {  
//                if( value) {
//                    start_stop_fuel.setDisable(true);
//                    
//                }
                night_mode_onoff.setSelected(value);
                night_mode_onoff.setDisable(!value);
                is_night_mode = value;               
            }});
    }
    
    private void SendStatus() throws UnknownHostException {
        
        if (is_fueling) {

            SendCurrentVolume();
        } 
        
        //Sending current state
        
        current_state_message.setCommand(current_state);
        current_state_message.setData(state);
        
        s.send(User.Kania_Filip_Wojciech, InetAddress.getByName("127.0.0.1"), 10_001, 
            mh.PrepareMessage(current_state_message));      
    }

    private void SendCurrentVolume() throws UnknownHostException {
        
        //Sending current fueling volume
        current_volume_message.setCommand(current_volume_byte);
        current_volume_message.setData(volume_df.format(current_volume));
            
        System.out.println(volume_df.format(current_volume));
            
        s.send(User.Kania_Filip_Wojciech, InetAddress.getByName("127.0.0.1"), 10_001, 
            mh.PrepareMessage(current_volume_message));
    }
    
}
