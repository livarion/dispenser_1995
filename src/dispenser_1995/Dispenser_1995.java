/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dispenser_1995;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author FILIP
 */
public class Dispenser_1995 extends Application {

    private FXMLLoader loader;
    private Parent root;
    
    @Override
    public void start(Stage primaryStage) throws IOException {
       
        loader = new FXMLLoader(getClass().getResource("/dispenser_1995/view/dispenser_view.fxml"));
        root = (Parent) loader.load();
        
        Scene scene = new Scene(root);
        
        primaryStage.setTitle("v0.1");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
